use std::collections::HashMap;
use std::hash::Hash;
use std::collections::hash_map::Entry::Occupied;

pub struct Node<K, V, W> {
    // Key, Value, Weight
    pub key: K,
    pub data: V,
    pub adj: HashMap<K, W>,
}

impl<K: Eq + Hash + Clone, V, W> Node<K, V, W> {
    pub fn new(key: K, data: V) -> Node<K, V, W> {
        Node {
            key: key,
            data: data,
            adj: HashMap::new(),
        }
    }
}

pub struct Graph<K, V, W> {
    nodes: HashMap<K, Node<K, V, W>>,
}

impl<K: Eq + Hash + Clone, V, W> Graph<K, V, W> {
    pub fn new() -> Graph<K, V, W> {
        Graph { nodes: HashMap::new() }
    }
    pub fn insert_node(&mut self, key: K, data: V) {
        self.nodes.insert(key.clone(), Node::new(key.clone(), data));
    }
    pub fn remove_node(&mut self, key: K) {
        self.nodes.remove(&key.clone());
        for (_, node) in self.nodes.iter_mut() {
            // Anything better than O(V) cost?
            node.adj.remove(&key.clone());
        }
    }
    pub fn insert_edge(&mut self, source: K, destination: K, weight: W) {
        match self.nodes.entry(source) {
            Occupied(ref mut node) => node.get_mut().adj.insert(destination, weight),
            _ => None,
        };
    }
    pub fn get(&self, key: K) -> Option<&Node<K, V, W>> {
        self.nodes.get(&key)
    }
}
